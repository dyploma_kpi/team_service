from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field


class CreateReview(BaseModel):
    profile_id: Annotated[int, Field(ge=1)]
    rate: Annotated[int, Field(ge=1, le=4)]
    desc: Annotated[str, MinLen(10), MaxLen(300)]
