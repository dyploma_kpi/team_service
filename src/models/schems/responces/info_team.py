from typing import Annotated
from annotated_types import MaxLen
from pydantic import BaseModel, Field


class InfoTeam(BaseModel):
    id: Annotated[int, Field(ge=1)]
    name: Annotated[str, MaxLen(100)]
    desc: Annotated[str, MaxLen(300)]
    recepient: Annotated[str, MaxLen(200)]
    drone: InfoDrone | None = None
