from src.db import FileDAL, file_dal, ProfileDAL, profile_dal
from src.utils import Verificator, verificator, FileManipulator, file_manipulator
from src.models.dal import Profile, File
from src.models.schems import InfoProfile, InfoFile
from src.config import settings
from fastapi import UploadFile
from fastapi import HTTPException, status
from pydantic import parse_obj_as



class FileService():
    def __init__(self, file_dal: FileDAL, profile_dal: ProfileDAL, file_manipulator: FileManipulator, verificator: Verificator) -> None:
        self._file_dal: FileDAL = file_dal
        self._profile_dal: ProfileDAL = profile_dal
        self._file_manipulator: FileManipulator = file_manipulator
        self._verificator: Verificator = verificator
        
    async def modify_avatar(self, file: UploadFile, token: str) -> Profile:
        user_id: int = await self._verificator.verify(token)
        avatar_url: str = await self._file_manipulator.upload_file(file, settings.s3_avatar_dir, str(user_id))
        profile: Profile = await self._profile_dal.update_avatar_url(user_id, avatar_url)
        if not profile:
            print("Profile didnt find")
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        return parse_obj_as(InfoProfile, vars(profile))
    
    async def upload_desc_files(self, files: list[UploadFile], token: str) -> list[InfoFile]:
        user_id: int = await self._verificator.verify(token)
        file_list: list[InfoProfile] = []
        for file in files:
            file_url: str = await self._file_manipulator.upload_file(file, settings.s3_desc_dir, f"files-{user_id}/{file.filename.split(".")[0]}")
            info_file: File = await self._file_dal.create_file(user_id, file_url)
            file_list.append(parse_obj_as(InfoFile, vars(info_file)))
        return file_list
    
    async def delete_desc_file(self, file_id: int, token: str) -> bool:
        user_id: int = await self._verificator.verify(token)
        file: File = await self._file_dal.get_file(file_id)
        if await self._file_manipulator.delete_file(file.file_url):
            return await self._file_dal.delete_file(file_id)
        raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        
    
    async def get_desc_files_by_profile_id(self, profile_id: int, token: str) -> list[InfoFile]:
        user_id: int = await self._verificator.verify(token)
        if user_id == profile_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Use another request to get information about yourself"
            )
        files: list[File] | None = await self._file_dal.get_files(profile_id)
        return parse_obj_as(list[InfoFile], [vars(file) for file in files])
    
    async def get_my_desc_files(self, token: str) -> list[InfoFile]:
        user_id: int = await self._verificator.verify(token)
        files: list[File] | None = await self._file_dal.get_files(user_id)
        return parse_obj_as(list[InfoFile], [vars(file) for file in files])
            
        
file_service = FileService(file_dal, profile_dal, file_manipulator, verificator)
