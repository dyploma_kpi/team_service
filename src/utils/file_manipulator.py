from src.config import settings
from fastapi import HTTPException, status, UploadFile
from aiobotocore.session import AioSession, get_session


class FileManipulator:
    def __init__(self, s3_access_key: str, s3_secret_key: str, s3_space_name: str, s3_region_name: str, s3_url: str) -> None:
        self._s3_access_key = s3_access_key
        self._s3_secret_key = s3_secret_key
        self._s3_space_name = s3_space_name
        self._s3_region_name = s3_region_name
        self._s3_url = s3_url
    
    async def upload_file(self, file: UploadFile, directory: str, file_name: str) -> str:
        session: AioSession = get_session()
        try:
            async with session.create_client("s3",
                                            region_name=self._s3_region_name,
                                            endpoint_url=self._s3_url,
                                            aws_access_key_id=self._s3_access_key,
                                            aws_secret_access_key=self._s3_secret_key) as client:
                file_content: bytes = await file.read()
                image_type: str = file.filename.split(".")[-1]
                file_name_type: str = f"{file_name}.{image_type}"
                await client.put_object(Bucket=directory, Key=file_name_type, Body=file_content, ACL="public-read", ContentType=f"image/{image_type}")
                return f"{self._s3_url}/{directory}/{file_name_type}"
        except Exception as e:
            print(str(e))
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def delete_file(self, file_url: str) -> bool:
        session: AioSession = get_session()
        try:
            async with session.create_client("s3",
                                            region_name=self._s3_region_name,
                                            endpoint_url=self._s3_url,
                                            aws_access_key_id=self._s3_access_key,
                                            aws_secret_access_key=self._s3_secret_key) as client:
                file_url = file_url.replace(f"{self._s3_url}/", "")
                items_of_file: list[str] = file_url.split("/")
                await client.delete_object(Bucket=items_of_file[0], Key="/".join(items_of_file[1:]))
                return True
        except Exception as e:
            print(str(e))
            return False
    
file_manipulator: FileManipulator = FileManipulator(settings.s3_access_key, settings.s3_secret_key, settings.s3_space_name, settings.s3_region_name, settings.s3_url)
