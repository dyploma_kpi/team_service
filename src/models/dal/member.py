from typing import TYPE_CHECKING
from sqlalchemy import String, DateTime, Integer, Text, ForeignKey, Boolean
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base
from src.config import settings

if TYPE_CHECKING:
    from src.models.dal import Team
    from src.models.dal import Role


class Member(Base):
    user_id: Mapped[int] = mapped_column(Integer, nullable=False)
    team_id: Mapped[int] = mapped_column(ForeignKey("teams.id"), nullable=False)
    role_id: Mapped[int] = mapped_column(ForeignKey("roles.id"), nullable=False)
    is_confirmed: Mapped[bool] = mapped_column(Boolean, nullable=False, server_default='0')

    team: Mapped["Team"] = relationship(back_populates="members", foreign_keys=[team_id])
    role: Mapped["Role"] = relationship(back_populates="members", foreign_keys=[role_id])
