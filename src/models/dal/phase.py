from typing import TYPE_CHECKING
from sqlalchemy import String, DateTime, Integer, Text, ForeignKey, Boolean
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base
from src.config import settings

if TYPE_CHECKING:
    from src.models.dal import Team
    from src.models.dal import Confirmation


class Phase(Base):
    name: Mapped[str] = mapped_column(String(100), nullable=False)
    team_id: Mapped[int] = mapped_column(ForeignKey("teams.id"), nullable=False)
    start_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    end_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), nullable=True)

    team: Mapped["Team"] = relationship(back_populates="phases", foreign_keys=[team_id])
    confirmations: Mapped[list["Confirmation"]] = relationship(back_populates="phase")
