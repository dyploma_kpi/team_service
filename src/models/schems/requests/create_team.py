from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field
from src.models.schems import CreateDrone


class CreateTeam(BaseModel):
    name: Annotated[str, MaxLen(100)]
    desc: Annotated[str, MaxLen(300)]
    recepient: Annotated[str, MaxLen(200)]
    drone: CreateDrone
