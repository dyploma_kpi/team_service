from sqlalchemy import select

from src.db import DatabaseHelper, db_helper
from src.models.dal import File

class FileDAL():
    
    def __init__(self, db_helper: DatabaseHelper) -> None:
        self._db_helper = db_helper
        

    async def create_file(self, profile_id: int, file_url: str) -> File:
        async with self._db_helper.session_factory() as session:
            try:
                file = File(profile_id=profile_id, file_url=file_url)
                session.add(file)
                await session.commit()
                await session.refresh(file)
                return file
            except Exception as e:
                print(f"Error create file: {e}")
                return None
            
    async def delete_file(self, file_id: int) -> bool:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(File).where(File.id == file_id)
                file: File | None = await session.scalar(stmt)

                await session.delete(file)
                await session.commit()
                return True
            except Exception as e:
                print(f"Error delete file: {e}")
                return False
            
    async def get_file(self, file_id: int) -> File:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(File).where(File.id == file_id)
                file: File | None = await session.scalar(stmt)

                await session.commit()
                return file
            except Exception as e:
                print(f"Error get file: {e}")
                return False
    
    async def get_files(self, profile_id: int) -> list[File]:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(File).where(File.profile_id == profile_id)
                files: list[File] | None = await session.scalars(stmt)

                await session.commit()
                return files
            except Exception as e:
                print(f"Error get files: {e}")
                return False


file_dal = FileDAL(db_helper)
    