from typing import TYPE_CHECKING
from sqlalchemy import String, DateTime, Integer, Text, ForeignKey
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base
from src.config import settings

if TYPE_CHECKING:
    from src.models.dal import Drone
    from src.models.dal import Member
    from src.models.dal import Phase


class Team(Base):
    name: Mapped[str] = mapped_column(String(100), nullable=False)
    desc: Mapped[str] = mapped_column(Text, nullable=False)
    avatar_url: Mapped[str] = mapped_column(String(300), nullable=False, server_default=settings.s3_default_avatar)
    recepient: Mapped[str] = mapped_column(String(200), nullable=False)
    drone_id: Mapped[int] = mapped_column(ForeignKey("drones.id"), unique=True, nullable=False)
    creation_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    modification_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), onupdate=func.now(), server_default=func.now())

    drone: Mapped["Drone"] = relationship(back_populates="team", foreign_keys=[drone_id])
    members: Mapped[list["Member"]] = relationship(back_populates="team")
    phases: Mapped[list["Phase"]] = relationship(back_populates="team")
