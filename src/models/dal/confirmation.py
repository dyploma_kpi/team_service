from typing import TYPE_CHECKING
from sqlalchemy import String, DateTime, Integer, Text, ForeignKey, Boolean
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base
from src.config import settings

if TYPE_CHECKING:
    from src.models.dal import Phase


class Confirmation(Base):
    phase_id: Mapped[int] = mapped_column(ForeignKey("phases.id"), nullable=False)
    results: Mapped[str] = mapped_column(Text, nullable=False)
    problems: Mapped[str] = mapped_column(Text, nullable=False)
    review_message: Mapped[str] = mapped_column(Text, nullable=True)
    review_result: Mapped[bool] = mapped_column(Boolean, nullable=True)
    sent_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    reviewed_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), nullable=True)

    phase: Mapped["Phase"] = relationship(back_populates="confirmations", foreign_keys=[phase_id])
