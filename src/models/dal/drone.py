from typing import TYPE_CHECKING
from sqlalchemy import Integer, Text, ForeignKey, String, Numeric, DateTime
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import func
from src.models.dal.base import Base
import datetime

if TYPE_CHECKING:
    from src.models.dal import Team


class Drone(Base):
    name: Mapped[str] = mapped_column(String(100), nullable=False)
    frame: Mapped[str] = mapped_column(String(100), nullable=False)
    camera: Mapped[str] = mapped_column(String(100), nullable=False)
    stack: Mapped[str] = mapped_column(String(100), nullable=False)
    engines: Mapped[str] = mapped_column(String(100), nullable=False)
    propellers: Mapped[str] = mapped_column(String(100), nullable=False)
    antenna: Mapped[str] = mapped_column(String(100), nullable=False)
    video_transmitter: Mapped[str] = mapped_column(String(100), nullable=False)
    battery: Mapped[str] = mapped_column(String(100), nullable=False)
    price: Mapped[float] = mapped_column(Numeric(12,2), nullable=True)
    creation_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    modification_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), onupdate=func.now(), server_default=func.now())

    team: Mapped["Team"] = relationship("Team", back_populates="drone")
