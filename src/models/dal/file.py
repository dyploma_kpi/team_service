from typing import TYPE_CHECKING
import datetime
from sqlalchemy import String, ForeignKey, DateTime
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import func

from src.models.dal.base import Base

if TYPE_CHECKING:
    from src.models.dal import Confirmation


class File(Base):
    confirmation_id: Mapped[int] = mapped_column(ForeignKey("confirmations.id"), unique=False, nullable=False)
    file_url: Mapped[str] = mapped_column(String(300), nullable=False)
    creation_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())

    confirmation: Mapped["Confirmation"] = relationship(back_populates="files", foreign_keys=[confirmation_id])
