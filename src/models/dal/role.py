from typing import TYPE_CHECKING

from sqlalchemy import String, Text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base

if TYPE_CHECKING:
    from src.models.dal import Member


class Role(Base):
    name: Mapped[str] = mapped_column(String(100), nullable=False)
    desc: Mapped[str] = mapped_column(Text, nullable=False)

    members: Mapped[list["Members"]] = relationship(back_populates="role")
