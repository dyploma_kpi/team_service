from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel


class UpdateDrone(BaseModel):
    name: Annotated[str, MaxLen(100)] | None = None
    frame: Annotated[str, MaxLen(100)] | None = None
    camera: Annotated[str, MaxLen(100)] | None = None
    stack: Annotated[str, MaxLen(100)] | None = None
    engines: Annotated[str, MaxLen(100)] | None = None
    propellers: Annotated[str, MaxLen(100)] | None = None
    antenna: Annotated[str, MaxLen(100)] | None = None
    video_transmitter: Annotated[str, MaxLen(100)] | None = None
    battery: Annotated[str, MaxLen(100)] | None = None
