import json
from httpx import AsyncClient, Response
from src.config import settings
from fastapi import HTTPException, status


class Verificator:
    def __init__(self, auth_api_url: str) -> None:
        self._auth_api_url = auth_api_url
    
    async def verify(self, token: str) -> int:
        if not token:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="auth header doesn`t find"
            )
        async with AsyncClient() as client:
            token_items: list[str] = token.split(" ")
            response: Response = await client.post(
                f"{self._auth_api_url}/verify",
                json={"token_type": token_items[0], "access_token": token_items[1]}
            )

            if response.status_code != status.HTTP_202_ACCEPTED:
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="You aren`t authorized"
                ) 
            return json.loads(response.text)["id"]
    
verificator = Verificator(settings.auth_api_url)
