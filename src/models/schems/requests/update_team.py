from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field
from src.models.schems import UpdateDrone


class UpdateTeam(BaseModel):
    id: Annotated[int, Field(ge=1)]
    name: Annotated[str, MaxLen(100)] | None = None
    desc: Annotated[str, MaxLen(300)] | None = None
    recepient: Annotated[str, MaxLen(200)] | None = None
    drone: UpdateDrone | None = None
